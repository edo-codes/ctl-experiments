#! /usr/bin/env deno

export type CTL<P> = P | boolean | Not<P> | And<P> | EX<P> | EU<P> | AU<P>;

type Primitive = number | string | boolean;

export type NotRep<P> = ["not", CTL<P>];
export interface Not<P> extends NotRep<P> {}
export type AndRep<P> = [CTL<P>, "and", CTL<P>];
export interface And<P> extends AndRep<P> {}
export type EXRep<P> = ["EX", CTL<P>];
export interface EX<P> extends EXRep<P> {}
export type EURep<P> = ["E", CTL<P>, "U", CTL<P>];
export interface EU<P> extends EURep<P> {}
export type AURep<P> = ["A", CTL<P>, "U", CTL<P>];
export interface AU<P> extends AURep<P> {}

export let not = <P>(phi: CTL<P>) /*   */ => ["not", phi] as CTL<P>;
export let and = <P>(phi: CTL<P>, psi: CTL<P>) => [phi, "and", psi] as CTL<P>;
export let EX = <P>(phi: CTL<P>) /*   */ => ["EX", phi] as CTL<P>;
export let E_U = <P>(phi: CTL<P>, psi: CTL<P>) =>
  ["E", phi, "U", psi] as CTL<P>;
export let A_U = <P>(phi: CTL<P>, psi: CTL<P>) =>
  ["A", phi, "U", psi] as CTL<P>;

export let or = <P>(phi: CTL<P>, psi: CTL<P>) => not(and(not(phi), not(psi)));
export let implies = <P>(phi: CTL<P>, psi: CTL<P>) => or(not(phi), psi);

export let AX = <P>(phi: CTL<P>) => not(EX(not(phi)));

export let EF = <P>(phi: CTL<P>) => E_U(true, phi);
export let AF = <P>(phi: CTL<P>) => A_U(true, phi);
export let EG = <P>(phi: CTL<P>) => not(AF(not(phi)));
export let AG = <P>(phi: CTL<P>) => not(EF(not(phi)));

export let AUtoEU2 = <P>([, phi, , psi]: AURep<P>) =>
  and(not(E_U(not(psi), and(not(phi), not(psi)))), not(EG(not(psi))));

export function isProp<P>(phi: CTL<P>): phi is P {
  return typeof phi === "string";
}

export function isBool<P>(phi: CTL<P>): phi is boolean {
  return typeof phi === "boolean";
}

export function isNot<P>(phi: CTL<P>): phi is Not<P> {
  return !isProp(phi) && !isBool(phi) && phi[0] === "not";
}

export function isAnd<P>(phi: CTL<P>): phi is And<P> {
  return !isProp(phi) && !isBool(phi) && phi[1] === "and";
}

export function isEX<P>(phi: CTL<P>): phi is EX<P> {
  return !isProp(phi) && !isBool(phi) && phi[0] === "EX";
}

export function isEU<P>(phi: CTL<P>): phi is EU<P> {
  return !isProp(phi) && !isBool(phi) && phi[0] === "E";
}

export function isAU<P>(phi: CTL<P>): phi is AU<P> {
  return !isProp(phi) && !isBool(phi) && phi[0] === "A";
}

export interface Kripke<S extends Primitive, P> {
  // states
  S: S[];
  // initial state
  s0: S;
  // arrows
  R: (s: S) => S[];
  // labels
  V: (s: S) => P[];
}

export function W_K<S extends Primitive, P>({ S, s0, R, V }: Kripke<S, P>) {
  let reverse_R = (s: S) => S.filter(t => R(t).includes(s));
  // console.log(
  //   "R(s) = ",
  //   S.flatMap(s => R(s).map(t => `${s} -> ${t}, `))
  // );
  // console.log(
  //   "reverse_R(s) = ",
  //   S.flatMap(s => reverse_R(s).map(t => `${s} <- ${t}, `))
  // );
  return function W(formula: CTL<P>): S[] {
    let W_prop = (phi: P) => S.filter(s => V(s).includes(phi));
    let W_bool = (phi: boolean) /*      */ => (phi ? [...S] : []);
    let W_not = (phi: CTL<P>) => S.filter(s => !W(phi).includes(s));
    let W_and = (phi: CTL<P>, psi: CTL<P>) => intersection(W(phi), W(psi));
    let W_EX = (phi: CTL<P>) =>
      S.filter(s => any(R(s), x => W(phi).includes(x)));
    let W_EU = (phi: CTL<P>, psi: CTL<P>) => {
      let Wphi = W(phi);
      let Wpsi = W(psi);
      let Z: S[];
      let Znext: S[] = Wpsi;
      do {
        Z = Znext;
        Znext = union(Z, Z.flatMap(reverse_R).filter(s => Wphi.includes(s)));
      } while (!setequality(Z, Znext));
      return Z;
    };
    let W_AU = (phi: CTL<P>, psi: CTL<P>) => {
      let Wphi = W(phi);
      let Wpsi = W(psi);
      let Z: S[];
      let Znext: S[] = Wpsi;
      do {
        Z = Znext;
        Znext = union(
          Wpsi,
          intersection(Wphi, S.filter(s => issubset(R(s), Z)))
        );
      } while (!setequality(Z, Znext));
      return Z;
    };

    let result;
    if (isProp(formula)) {
      let phi = formula;
      result = W_prop(phi);
    } else if (isBool(formula)) {
      let phi = formula;
      result = W_bool(phi);
    } else if (isNot(formula)) {
      let [, phi] = formula;
      result = W_not(phi);
    } else if (isAnd(formula)) {
      let [phi, , psi] = formula;
      result = W_and(phi, psi);
    } else if (isEX(formula)) {
      let [, phi] = formula;
      result = W_EX(phi);
    } else if (isEU(formula)) {
      let [, phi, , psi] = formula;
      result = W_EU(phi, psi);
    } else if (isAU(formula)) {
      let [, phi, , psi] = formula;
      result = W_AU(phi, psi);
    } else {
      throw new Error();
    }

    console.log("W(", JSON.stringify(formula), ") = ", result);
    return result;
  };
}

export function holds<S extends Primitive, P>(K: Kripke<S, P>, phi: CTL<P>) {
  console.log("K |=", JSON.stringify(phi));
  let result = W_K(K)(phi).includes(K.s0);
  console.log(result);
  return result;
}

function intersection<T extends Primitive>(a: Array<T>, b: Array<T>) {
  return a.filter(x => b.includes(x));
}

function union<T>(a: Array<T>, b: Array<T>) {
  return [...new Set([...a, ...b])];
}

function intersectionnotempty<T extends Primitive>(a: Array<T>, b: Array<T>) {
  a.findIndex(x => b.includes(x)) !== -1;
}

function issubset<T extends Primitive>(a: Array<T>, b: Array<T>) {
  for (let x of a) if (!b.includes(x)) return false;
  return true;
}

function setequality<T extends Primitive>(a: Array<T>, b: Array<T>) {
  for (let x of a) {
    if (!b.includes(x)) return false;
  }
  for (let y of b) {
    if (!a.includes(y)) return false;
  }
  return true;
}

function any<T>(a: Array<T>, p: (x: T) => boolean) {
  for (let x of a) {
    if (p(x)) return true;
  }
  return false;
}

function subformulas<P>(phi: CTL<P>): CTL<P>[] {
  if (isProp(phi)) return [phi as CTL<P>];
  else if (isNot(phi)) return [phi as CTL<P>].concat(subformulas(phi[1]));
  else if (isAnd(phi))
    return [phi as CTL<P>]
      .concat(subformulas(phi[0]))
      .concat(subformulas(phi[2]));
  else if (isEX(phi)) return [phi as CTL<P>].concat(subformulas(phi[1]));
  else if (isEU(phi))
    return [phi as CTL<P>]
      .concat(subformulas(phi[1]))
      .concat(subformulas(phi[3]));
  else if (isAU(phi))
    return [phi as CTL<P>]
      .concat(subformulas(phi[1]))
      .concat(subformulas(phi[3]));
  throw new Error();
}
