import {
  Kripke,
  holds,
  EX,
  and,
  E_U,
  CTL,
  or,
  not,
  A_U,
  AF,
  AG,
} from "./ctl.ts";

type M_S = 0 | 1 | 2 | 3;
type M_P = "p" | "q";
let M: Kripke<M_S, M_P> = {
  S: [0, 1, 2, 3],
  s0: 0,
  R: s => {
    switch (s) {
      case 0:
        return [0, 1];
      case 1:
        return [2];
      case 2:
        return [0, 1];
      case 3:
        return [2, 3];
    }
  },
  V: s => {
    switch (s) {
      case 0:
        return ["p"];
      case 1:
        return ["p"];
      case 2:
        return ["p", "q"];
      case 3:
        return [];
    }
  },
};

let infed: Kripke<
  0 | 1 | 2 | 3 | 4 | 5 | 6 | 7,
  "x" | "y" | "z"
> = {
  S: [0, 1, 2, 3, 4, 5, 6, 7],
  s0: 0,
  R: s => {
    switch (s) {
      case 0:
        return [2, 4];
      case 1:
        return [5];
      case 2:
        return [4, 1];
      case 3:
        return [0, 7];
      case 4:
        return [1];
      case 5:
        return [1];
      case 6:
        return [3, 4];
      case 7:
        return [5, 6];
    }
  },
  V: s => {
    switch (s) {
      case 0:
        return ["x", "y", "z"];
      case 1:
        return ["x", "y"];
      case 2:
        return ["y", "z"];
      case 3:
        return ["x"];
      case 4:
        return ["y"];
      case 5:
        return ["x", "z"];
      case 6:
        return ["z"];
      case 7:
        return [];
    }
  },
};

holds(M, A_U("p", and("p", "q")));
